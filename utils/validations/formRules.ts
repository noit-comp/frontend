export function requiredFieldRule(translateContext: any) {
  return (value: string) =>
    !!value || translateContext.$t("log-reg.rules.field_req");
}

// DB restriction
export function maxLenFieldRule(translateContext: any) {
  return (value: string) =>
    (!!value && value.length <= 128) ||
    translateContext.$t("log-reg.rules.field_max");
}

function nameFormatFule(translateContext: any) {
  return (value: string) =>
    (!!value && /^[А-Я][а-я]*$/.test(value)) ||
    translateContext.$t("log-reg.rules.name_format");
}

function emailFormatRule(translateContext: any) {
  return (value: string) =>
    (!!value && /.+@.+\..+/.test(value)) ||
    translateContext.$t("log-reg.rules.email_format");
}

function usernameFormatRule(translateContext: any) {
  return (value: string) =>
    (!!value && /^\w*$/.test(value)) ||
    translateContext.$t("log-reg.rules.username_format");
}

function usernameMinLenRule(translateContext: any) {
  return (value: string) =>
    (!!value && value.length >= 4) ||
    translateContext.$t("log-reg.rules.username_min");
}

function usernameMaxLenRule(translateContext: any) {
  return (value: string) =>
    (!!value && value.length <= 32) ||
    translateContext.$t("log-reg.rules.username_max");
}

// function passwordFormatRule(translateContext: any) {
//   return (value: string) =>
//     (!!value && /^[A-Za-z]\w{6,15}$/.test(value)) ||
//     translateContext.$t("log-reg.rules.password_format");
// }

function passwordMinLenRule(translateContext: any) {
  return (value: string) =>
    (!!value && value.length >= 8) ||
    translateContext.$t("log-reg.rules.password_min");
}

function passwordMatchRule(translateContext: any, originalPass: String) {
  return (value: string) =>
    (!!value && value === originalPass) ||
    translateContext.$t("log-reg.rules.password_no_match");
}

export function nameRules(translateContext: any) {
  return [
    requiredFieldRule(translateContext),
    nameFormatFule(translateContext),
    maxLenFieldRule(translateContext)
  ];
}

export function emailRules(translateContext: any) {
  return [
    requiredFieldRule(translateContext),
    emailFormatRule(translateContext),
    maxLenFieldRule(translateContext)
  ];
}

export function usernameRules(translateContext: any) {
  return [
    requiredFieldRule(translateContext),
    usernameFormatRule(translateContext),
    usernameMinLenRule(translateContext),
    usernameMaxLenRule(translateContext)
  ];
}

export function passwordRules(translateContext: any) {
  return [
    requiredFieldRule(translateContext),
    passwordMinLenRule(translateContext),
    maxLenFieldRule(translateContext)
  ];
}

export function passwordReRules(translateContext: any, originalPass: String) {
  return [
    requiredFieldRule(translateContext),
    passwordMatchRule(translateContext, originalPass)
  ];
}
