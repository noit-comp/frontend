import { AxiosError } from "axios";
import consola from "consola";

export const errorHandlerMixin = {
  methods: {
    displayError(err: AxiosError, erorrMessages: Record<number, string>) {
      let errorMessage;
      if (err.response === undefined) {
        errorMessage = this.$t("popups.requestError");
      } else {
        const errorID: string =
          erorrMessages[err.response.status] || this.$t("popups.requestError");
        errorMessage = this.$t(errorID);
      }

      this.$toast.error(errorMessage);
      consola.error(err);
    }
  }
};
