# Coding Guidelines

- We use [eslint](https://eslint.org/) and [prettier](https://prettier.io/)
  for code formating and linting. We use eslint typescript module.
- You can validate that your code complies with these guidelines by running \
  `yarn lint`
- Here are some helpfull resources for:
  - General
    - [Learn nuxt ts](https://github.com/pecataToshev/learn-nuxt-ts)
  - Testing
    - [Vue Test Utils](https://vue-test-utils.vuejs.org/api/options.html)
    - [shallowMount](https://github.com/vuejs/vue-test-utils/issues/1013)
