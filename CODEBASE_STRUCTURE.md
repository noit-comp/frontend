We use the same directory structure as described in
[nuxt](https://nuxtjs.org/guide/directory-structure/). \
Additional directories are:


| Directory | Description |
| --- | --- |
| [test/e2e](test/e2e) | e2e test source folder. |
| [test/unit](test/unit) | Unit testing source folder.  |