# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

---

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

---

## [0.1.0] - 2020-10-25

### Added

- Add more repo docs and update README (#17)
- Add logo and favicon (#19)
- Add lint stage to CI
- Add footer details
- Add user input validation
- Add notifications

### Changed

- Change Docker test image
- Update dependencies

### Fixed

- Fix sidebar on small devices
- CI on deploy and node image

---

## [v0.0.8] - 2020-05-31

### Added

### Changed

### Fixed

- Fix integration tests
- Fix unit tests
- Fix CI

---

## [v0.0.7] - 2020-05-15

Initial release

### Added

- Added page structure
- Setup project basics

### Changed

### Fixed
