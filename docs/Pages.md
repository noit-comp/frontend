# Endpoints

This files contains data abaout endpoints



| path | visible for | description |
| ---- | ----------- | ----------- |
| / | public | Displays list of latest news (short) |
| /news/  | public | Display full list of news (long) |
| /news/:newsId  | public | Display full data of news with ID |
| /rules | public | Display current competition rules |
| /topics | public | Display current competition topics |
| /archive | public | List all past years/seasons |
| /archive/:year | public | List all projects for :year |
| /project/:projectId | authenticated | project specific data (different for different user types |
| /project/new | student | Create new project form |
| /student | student | list all projects related to authenticated student |
| /committee | committee | List all committee groups |
| /faq | public | list of frequently asked questions |
