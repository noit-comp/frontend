export const state = () => ({
  leftSidebar: {
    clipped: true,
    drawer: true,
    miniVariant: false
  },
  offset: true,
  title: "layout.title",
  versioning: {
    backend: {
      version: ""
    },
    frontend: {
      version: process.env.NUXT_ENV_VERSION,
      buildTime: process.env.NUXT_ENV_BUILD_TIME
    }
  }
});

export const getters = {
  getLeftSidebarClipped: (state: any) => state.leftSidebar.clipped,
  getLeftSidebarDrawer: (state: any) => state.leftSidebar.drawer,
  getLeftSidebarMiniVariant: (state: any) => state.leftSidebar.miniVariant,
  getTitle: (state: any) => state.title,
  getOffset: (state: any) => state.offset,
  getBackendVersion: (state: any) => state.versioning.backend.version,
  getFrontendVersion: (state: any) => state.versioning.frontend.version,
  getFrontendVersionBuildTime: (state: any) =>
    state.versioning.frontend.buildTime
};

export const mutations = {
  toggleLeftSidebarMiniVariant(state: any) {
    state.leftSidebar.miniVariant = !state.leftSidebar.miniVariant;
  },
  toggleLeftSidebarDrawer(state: any) {
    state.leftSidebar.drawer = !state.leftSidebar.drawer;
  },
  calculateInitialDrawer(state: any, { vuetify }: any) {
    state.leftSidebar.drawer = isBigScreen(vuetify);
  }
};

function isBigScreen(vuetify: any): boolean {
  return vuetify.breakpoint.lgAndUp;
}

export const actions = {
  handleNavIconClick(context: any, { vuetify }: any): void {
    if (isBigScreen(vuetify)) {
      context.commit("toggleLeftSidebarMiniVariant");
    } else {
      context.commit("toggleLeftSidebarDrawer");
    }
  },
  getBackendVersion(state: any, { axios }: any): void {
    axios
      .get("/version")
      .then((res: any) => {
        state.versioning.backend.version = res.data.appVersion;
      })
      .catch(() => {
        setTimeout(this.getBackendVersion, 10000);
      });
  }
};
