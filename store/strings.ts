import axios from "axios";
import consola from "consola";

// Initialize store with all parameters and init values for the module.
export const state = () => ({
  list: []
});

// Read-only functions which we can use for this module
export const getters = {
  totalStrings: (state: any) => {
    return state.list.length;
  }
};

// Functions we can use in components to change our state.
// Important: Must be synchronous!
export const mutations = {
  add(state: any, text: String) {
    state.list.push({
      text
    });
  },
  set(state: any, newList: Array<String>) {
    state.list = newList;
  },
  remove(state: any, { todo }: { todo: String }) {
    state.list.splice(state.list.indexOf(todo), 1);
  }
};

// Asynchronous functions we can use to fetch some data
// and call mutations to fill our store.
export const actions = {
  // Fills strings with names of people
  async fillWithNames(context: any, nOfStringsToPass: number) {
    // Fetch maximum 10 users
    nOfStringsToPass = nOfStringsToPass > 10 ? 10 : nOfStringsToPass;
    await axios
      .get("https://jsonplaceholder.typicode.com/users/")
      .then(res => {
        const strArr = res.data.splice(0, nOfStringsToPass).map((user: any) => {
          return { text: user.name };
        });
        context.commit("set", strArr);
      })
      .catch(err => consola.error(err));
  }
};
