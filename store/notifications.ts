// import axios from "axios";
// import consola from "consola";
import { GetterTree, MutationTree } from "vuex";
import NOTIFICATIONS from "~/data/dummy-data";

// Initialize store with all parameters and init values for the module.
export const state = () => ({
  notifications: NOTIFICATIONS,
  selectedNotification: undefined
});

export type RootState = ReturnType<typeof state>;

// Read-only functions which we can use for this module
export const getters: GetterTree<RootState, RootState> = {
  getNotifications: state => {
    return state.notifications;
  },
  getSelectedNotification: state => {
    return state.selectedNotification;
  },
  getUnreadCount: state => {
    return state.notifications.filter(n => !n.read).length;
  }
};

// Functions we can use in components to change our state.
// Important: Must be synchronous!
export const mutations: MutationTree<RootState> = {
  setSelected(state, selected) {
    state.selectedNotification = selected;
  },
  // will be async API call when backend supports such change
  setRead(state, notification) {
    state.notifications
      .filter(n => n.id === notification.id)
      .forEach(n => (n.read = true));
  }
};

// // Asynchronous functions we can use to fetch some data
// // and call mutations to fill our store.
// export const actions = {
//   // Fills strings with names of people
//   async fillWithNames(context: any, nOfStringsToPass: number) {
//     // Fetch maximum 10 users
//     nOfStringsToPass = nOfStringsToPass > 10 ? 10 : nOfStringsToPass;
//     await axios
//       .get("https://jsonplaceholder.typicode.com/users/")
//       .then(res => {
//         const strArr = res.data.splice(0, nOfStringsToPass).map((user: any) => {
//           return { text: user.name };
//         });
//         context.commit("set", strArr);
//       })
//       .catch(err => consola.error(err));
//   }
// };
