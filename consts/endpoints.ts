export const authURI = {
  register: "/auth/register"
};

export const updateUserURI = {
  email: "/user/mail",
  password: "/user/password"
};
