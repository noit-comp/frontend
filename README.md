## Table of Contents

- [About](#about)
- [Getting Started](#gettingStarted)
- [Deployment](#deployment)
- [Built Using](#built_using)
- [Contributing](../CONTRIBUTING.md)

## About <a name = "about"></a>

This project is part from NOIT Competition System. This repo contains
all code and tests related to frontend module.

## Getting Started <a name = "gettingStarted"></a>

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See
[deployment](#deployment) for notes on how to deploy the project on a live
system.

### Requirements

Before running in development mode you should have running instance of
backend and news servers. Both should be accessed via network protocols.

### Installing and running

Here is a tutorial how to get a development env running.
First you should download this repo (via git or download a release).
Then you can start this software with proper yarn executable

```
yarn install
yarn run dev
```

After a while project should be working on http://localhost:3000
or what you set in `NUXT_ENV_BASE_URL`

## Running the tests <a name = "tests"></a>

### Unit testing

```
yarn unitTest
```

### Integration testing

```
yarn integrationTest
```

### Coding style tests

Test provided code style. Code should be written as shown in
[CODING_GUIDELINES.md](CODING_GUIDELINES.md)

```
yarn lint
```

## Deployment <a name = "deployment"></a>

One should read our [environment variables](ENVIRONMENT_VARIABLES.md) which
should be set before build.

We suggest to use static files generation. Since nuxt 2.13 `yarn generate`
has beeen changed with

```
yarn build
yarn export
```

It creates a `dist` folder with static files which should be hosted with nginx
or Apache. It could be run also with

```
yarn build
yarn start
```

But we do not like this way.

For **PRODUCTION** use with git (CI/CD).

## Built Using <a name = "built_using"></a>

- [Nuxt.js](https://nuxtjs.org/) - Vue framework
- [Nuxt Typescript](https://typescript.nuxtjs.org/) - TypeScript Support
  for Nuxt.js
- [Jest](https://jestjs.io/) - Unit testing framework
- [Cypress](https://www.cypress.io/) - e2e testing
