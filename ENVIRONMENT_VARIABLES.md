# Environment variables
We have created some variables for easy configuration and removing data
duplication. We strongly recommend redefining ***at least DB_PASS in 
PRODUCTION***

| Variable | Default | Description |
| ------  | ------ | ------ | 
| NUXT_ENV_BASE_URL | http://localhost:3000/ | Site url |
| NUXT_ENV_BACKEND_URL | http://localhost:8080/ | Backend url |
| NUXT_ENV_VERSION |  | We use this as commit short sha |
| NUXT_ENV_BUILD_TIME |  | Time when ```yarn build``` was executed |
