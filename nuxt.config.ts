import colors from "vuetify/es5/util/colors";
require("dotenv").config();
export default {
  ssr: false,
  target: "static",
  env: {
    baseUrl: process.env.BASE_URL || "http://localhost:3000"
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s - " + process.env.npm_package_name,
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.svg" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // '~/plugins/vuetify'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    "@nuxtjs/vuetify",
    /** @see https://typescript.nuxtjs.org/migration.html */
    "@nuxt/typescript-build"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    // '@nuxtjs/vuetify', // Causes error when build :/
    "nuxt-i18n",
    "@nuxtjs/auth",
    "@nuxtjs/toast"
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.NUXT_ENV_BACKEND_URL || "http://localhost:8080/"
  },
  /**
   *  https://auth.nuxtjs.org/
   */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: "/auth/login", method: "post", propertyName: false },
          logout: false,
          user: { url: "/auth/user", method: "get", propertyName: false }
        }
      }
    },
    redirect: {
      login: "/sign",
      logout: "/",
      /* ? */ callback: "/",
      home: "/"
    }
  },
  router: {
    middleware: ["auth"]
  },
  /*   ** nuxt-i18n module
   ** https://nuxt-community.github.io/nuxt-i18n/
   */
  i18n: {
    locales: [
      {
        code: "bg",
        file: "bg-BG.json"
      }
    ],
    lazy: true,
    langDir: "lang/",
    defaultLocale: "bg"
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    // customVariables: ['~/assets/variables.scss'],
    icons: {
      iconfont: "md" // 'mdiSvg' || 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
    theme: {
      options: {
        // generates ccs variables for the colours that can be used in the <style> block
        customProperties: true
      },
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.base,
          hover: "#545454",
          unread: "#333333",
          selected: "#6595a1"
        },
        light: {
          primary: colors.blue.lighten2,
          accent: colors.grey.lighten3,
          secondary: colors.amber.lighten3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.base,
          hover: "#f2f2f2",
          unread: "#d6d6d6",
          selected: "#e6faff"
        }
      }
    }
  },
  /** typescript config for nuxt */
  typescript: {
    typeCheck: false,
    ignoreNotFoundWarnings: true
  },
  toast: {
    closeButton: true,
    position: "top-right",
    duration: 3000,
    progressBar: true,
    preventDuplicates: true
  },
  /** Build configuration */
  build: {
    // extend (config: any, context: Context) { }
  }
};
