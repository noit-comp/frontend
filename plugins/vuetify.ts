import Vue from "vue";
import Vuetify from "vuetify";
import colors from "vuetify/lib/util/colors";
// import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import { Context } from "@nuxt/types";

Vue.use(Vuetify);

export default (ctx: Context) => {
  const vuetify = new Vuetify({
    icons: {
      iconfont: "md" // default - only for display purposes
    },
    theme: {
      dark: false,
      options: {
        minifyTheme: css => {
          return process.env.NODE_ENV === "production"
            ? css.replace(/[\r\n|\r|\n]/g, "")
            : css;
        }
      },
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.base,
          hover: "#545454",
          unread: "#333333",
          selected: "#6595a1"
        },
        light: {
          primary: colors.blue.lighten2,
          accent: colors.grey.lighten3,
          secondary: colors.amber.lighten3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.base,
          hover: "#f2f2f2",
          unread: "#d6d6d6",
          selected: "#e6faff"
        }
      }
    }
  });

  ctx.app.vuetify = vuetify;
  // @ts-ignore
  ctx.$vuetify = vuetify.framework;
};
