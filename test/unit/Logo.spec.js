import { shallowMount } from "@vue/test-utils";
import Logo from "../../components/Logo";

function factory(dark = false) {
  return shallowMount(Logo, {
    mocks: {
      $vuetify: {
        theme: {
          dark
        }
      }
    }
  });
}

describe("Logo", () => {
  test("mounts properly", () => {
    const wrapper = factory();
    expect(wrapper.vm).toBeTruthy();
  });

  test("renders properly", () => {
    const wrapper = factory();
    expect(wrapper.isVisible).toBeTruthy();
    expect(wrapper.element.tagName).toBe("svg");
  });

  function testColot(isDark, color) {
    const wrapper = factory(isDark);
    expect(wrapper.find("g").attributes().fill).toBe(color);
  }

  test("white color on dark theme", () => {
    testColot(true, "#ffffff");
  });

  test("black color on light theme", () => {
    testColot(false, "#000000");
  });
});
