describe("Test logo functionality", () => {
  it("Is visible", () => {
    cy.visit("/");
    expect(cy.get("#AppMainLogo")).to.exist;
  });

  it("On click redirect", () => {
    cy.visit("/sign");
    cy.get("#AppMainLogo").click();
    cy.location("pathname").should("eq", "/");
  });
});
