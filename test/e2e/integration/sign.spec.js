describe("Sign In/Up page tests", () => {
  const pageUrl = "/sign";
  function checkTitle(title) {
    cy.contains("h3", title);
  }
  it("Visits sign page for login", () => {
    cy.visit(pageUrl);
    checkTitle("Вход");
  });

  it("Visits sign page for registration", () => {
    cy.visit(pageUrl + "?register");
    checkTitle("Регистрация");
  });

  // it('increase counter', () => {
  //   cy.viewport(1280, 1080)

  //   // cy.get('[type="text"]').type('10')
  //   // cy.get('[type="button"]').click({ multiple: true })

  //   // cy.contains('.v-alert.success div', '25')
  // })
});
