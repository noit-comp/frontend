export class Notification {
  id: string;
  date: string;
  subject: string;
  author: string;
  text: string;
  read: boolean;

  constructor(
    id: string,
    date: string,
    subject: string,
    author: string,
    text: string
  ) {
    this.id = id;
    this.date = date;
    this.author = author;
    this.subject = subject;
    this.text = text;
    this.read = false;
  }
}

export interface NotificationState {
  notifications: Notification[];
  selectedNotification: Notification | undefined;
  numUnread: number;
}
