export class NavigationLink {
  public readonly icon: string;
  public readonly localizationKey: string;
  public readonly to: string;
  public readonly requireLogIn: boolean;

  constructor(
    icon: string,
    localizationKey: string,
    to: string,
    requireLogIn: boolean = true
  ) {
    this.icon = icon;
    this.localizationKey = localizationKey;
    this.to = to;
    this.requireLogIn = requireLogIn;
  }
}

export const links: NavigationLink[] = [
  new NavigationLink("apps", "layout.sidebar.welcome", "/"),
  new NavigationLink("unsubscribe ", "layout.sidebar.news", "/news"),
  new NavigationLink("format_list_bulleted", "layout.sidebar.rules", "/rules"),
  new NavigationLink("adjust", "layout.sidebar.topics", "/topics"),
  new NavigationLink("archive", "layout.sidebar.archive", "/archive")
];
