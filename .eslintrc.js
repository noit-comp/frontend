module.exports = {
  extends: [
    "@nuxtjs/eslint-config-typescript",
    "plugin:chai-friendly/recommended",
    "plugin:prettier/recommended"
  ],
  globals: {
    cy: "readonly"
  },
  plugins: ["chai-friendly"],
  rules: {
    "prettier/prettier": [
      "error",
      {
        endOfLine: "auto"
      }
    ]
  }
};
